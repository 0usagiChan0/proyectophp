<?php
//print_r($_SERVER);

//validar q request sea de tipo post
if ($_SERVER['REQUEST_METHOD'] != 'POST'){
	die('Tipo de request no permitido');
}

//validar q existan datos usuario y password
if (!isset($_POST['usuario']) || !isset($_POST['password'])){
	die('No se ha especificado usuario o password');
}

//leer datos del $_POST
$usuario = $_POST['usuario'];
$password = $_POST['password'];

//inicio sesion php
session_start();

//leer referer
$referer = $_SERVER['HTTP_REFERER'];

//validar datos correctos
if ($usuario != 'admin' && $password != '123456'){
	$_SESSION['error'] = 'El usuario o password ingresado es incorrecto.';
}else{
	//iniciar session de membresia
	$_SESSION['usuario'] = $usuario;
}

//redirigir al referer
header("location: $referer");
?>